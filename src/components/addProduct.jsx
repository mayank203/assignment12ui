import React, {Component} from "react";
import auth from "../Services/authServices";
import http from "../Services/service";


class AddProduct extends Component {
    state ={
      Product : {name: "",price: "",category: "",imgLink: [],description: ""},
      link1:"",
      link2:"",
      link3:"",

    };
    
    handelChange = (e)=>{
        const {currentTarget : input} =e;
        let s1 = {...this.state};
        s1[input.name]=input.value
        s1.Product[input.name]=input.value;
        this.setState(s1);
    };   

    async postData(url) {
      const s1= {...this.state}
       let obj = s1.Product;
        console.log(url,obj);
        if(obj.name && obj.imgLink && obj.description && obj.price && obj.category){
        let response =await http.post(url,obj);
        let {data} = response;
        console.log(data);
        this.props.history.push("/manageProducts"); 
        }else{
          this.setState({err : "Fill all Fileds (*) First before Sumbit"});
        }
    }

    handelSumbit= (e)=>{
        e.preventDefault();
        const s1= {...this.state.Product}
        s1.imgLink.push(s1.link1); 
        s1.imgLink.push(s1.link2); 
        s1.imgLink.push(s1.link2); 
        this.setState(s1);
        this.postData(`product`);
    };

    render(){
        let {name,description,price,imgLink,category} = this.state.Product;
        let {err,link1,link2,link3} = this.state;
     return  <div className="container">
            {err && <span className="text-danger">{err}</span> }
            <div className="form-group m-2">
            <lable>Name <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Product Name"
               className="form-control" 
               id="name" name="name" 
               value={name}  
               onChange ={this.handelChange}>
               </input>
            </div>    
            <div className="form-group m-2">
             <lable>Description<span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Product Description"
               className="form-control" 
               id="description" name="description" 
               value={description}  
               onChange ={this.handelChange}>    
               </input>
            </div>

            <div className="form-group m-2">
             <lable>Price <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Product Price"
               className="form-control" 
               id="price" name="price" 
               value={price}  
               onChange ={this.handelChange}>    
               </input>
            </div>
            <div className="form-group m-2">
            <lable>Image Url <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Product Image URL 1"
               className="form-control" 
               id="link1" name="link1" 
               value={link1}  
               onChange ={this.handelChange}>
               </input>
               <input 
               type="text" 
               placeholder="Enter Product Image URL 2"
               className="form-control" 
               id="link2" name="link2" 
               value={link2}  
               onChange ={this.handelChange}>
               </input>
               <input 
               type="text" 
               placeholder="Enter Product Image URL 3"
               className="form-control" 
               id="link3" name="link3" 
               value={link3}  
               onChange ={this.handelChange}>
               </input>
            </div>
            <div className="form-group m-2">
            <lable> Category <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Product category"
               className="form-control" 
               id="category" name="category" 
               value={category}  
               onChange ={this.handelChange}>
               </input>
            </div>
            <button className="btn btn-primary m-2" onClick={this.handelSumbit}>Add</button>
            <br/>
          </div>
    }
}
export default AddProduct;