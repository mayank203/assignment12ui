import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "../Services/service";
import SaleImage from "./Saleimage";
import auth from "../Services/authServices";



class AllOrders extends Component{

     state = {
              data:[],
             };

   async componentDidMount(){
        let responce = await http.get(`orders`);
        let {data} = responce;
        console.log(data);
        this.setState({data:data});
    };

    render(){
        let {data} =this.state
                return (
                    <div className="container">
                     <SaleImage />
                     <div className="border">
                     <h3 className="text m-2">List of Order</h3> 
                                          
                     <div className="container text-center">
                     <div className="row text-light m-2">
                         <div className="col-3 bg-dark">Name</div>
                         <div className="col-2 bg-dark">City</div>
                         <div className="col-3 bg-dark">Address</div>
                         <div className="col-2 bg-dark">Amount</div>
                         <div className="col-2 bg-dark">Items</div>
                     </div>
                     {data.map((pr,index)=>(
                        <div className="row">
                         <div className="col-3 border">{pr.name}</div>
                         <div className="col-2 border">{pr.city}</div>
                         <div className="col-3 border">{pr.address}</div>
                         <div className="col-2 border">Rs.{pr.totalPrice}</div>
                         <div className="col-2 border">{pr.items.length}</div>
                     </div>
                     ))}
                     </div>
                     </div>
                    </div>
                    
                )
            }
        }
        export default AllOrders;
        
        