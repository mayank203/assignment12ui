import React, { Component } from "react";
import { Switch , Route , Redirect } from "react-router-dom";
import auth from "../Services/authServices";
import NavBar from "./navbar";
import Home from "./home";
import Login from "./login";
import Logout from "./logout";
import MyCart from "./myCart";
import CheckOut from "./CheckOutOrder"
import Thanku from "./thanksYou";
import AllOrders from './AllOrders'
import ManageProducts from "./manageProducts";
import AddProduct from './addProduct';
import EditProduct from "./editProduct";

class MainComponent extends Component {   
   state ={
       myCart:[],
       log:false
   }

   AddToCart =(items)=>{
     let s1 = {...this.state}
     s1.myCart.push(items);
     this.setState(s1);
   }
   RemoveCart=(id)=>{
    let s1 = {...this.state}
    let index = s1.myCart.findIndex((pr)=>pr.id===id);
    s1.myCart.splice(index,1);
    this.setState(s1);
   }

   handleQtyhome=(n,id)=>{
    let s1 = {...this.state}
    let index=s1.myCart.findIndex((p)=>p.id===id);
    this.handleQty(n,index);
   }

   handleQty=(n,index)=>{
       console.log(n,index)
       let s1 = {...this.state}
       if(n==="1"){
         s1.myCart[index].qty = s1.myCart[index].qty+1;
         s1.myCart[index].totalPrice = s1.myCart[index].price * s1.myCart[index].qty;
       }
       if(n==="-1"){
        s1.myCart[index].qty = s1.myCart[index].qty-1;
        if(!s1.myCart[index].qty){
          s1.myCart.splice(index,1);
        }else{
        s1.myCart[index].totalPrice = s1.myCart[index].price * s1.myCart[index].qty;
    }
      }
      console.log(s1.myCart)
      this.setState(s1);
   }

   login=()=>{
     let s1={...this.state}
     s1.log=true;
     this.setState(s1)
   }

    render() {
        const user = auth.getUser();
        let {myCart} =this.state;
        return (
            <div className="counter">
            <NavBar user={user} myCart={myCart}/>
            <Switch>
            <Route path="/checkOut" render={props=> user ? <CheckOut {...props} myCart={myCart} user={user} /> : <Redirect to ="/login" />}/>
            <Route path="/myCart" render={props=>  <MyCart handleQty={this.handleQty} myCart={myCart}  {...props}/>}/>
            <Route path="/myOrders" render={props=> user ? <AllOrders {...props} myCart={myCart} user={user} /> : <Redirect to ="/login" />}/>
            <Route path="/manageProducts" render={props=> user ? <ManageProducts {...props} user={user} /> : <Redirect to ="/login" />}/>
            <Route path="/addProduct" render={props=> user ? <AddProduct {...props} user={user} /> : <Redirect to ="/login" />}/>
            <Route path="/editProduct/:id" render={props=> user ? <EditProduct {...props} user={user} /> : <Redirect to ="/login" />}/>
            <Route path="/login" render={props=> <Login {...props}  login={this.login} />}/>
            <Route path="/logout" component={Logout}/>
            <Route path="/Thanks" component={Thanku}/>
            <Route path="/home/:category" render={props=> <Home handleQtyhome={this.handleQtyhome} myCart={myCart} AddToCart={this.AddToCart} RemoveCart={this.RemoveCart} {...props}  />}/>
            <Redirect from="/" to="/home/all" />
            </Switch>
            </div>
        )
    }
}
export default MainComponent;