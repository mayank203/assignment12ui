import React, {Component} from "react";
import http from '../Services/service';
import auth from "../Services/authServices";


class Login extends Component {
    state ={
      from : {email: "",password: ""},
    };
    
    handelChange = (e)=>{
        const {currentTarget : input} =e;
        let s1 = {...this.state};
        s1.from[input.name]=input.value;
        this.setState(s1);
    }; 
    
   async postData(url,obj){
       try {
       let response = await http.post(url,obj);
       let {data} = response;
       console.log("Logni Data" , data);
       auth.login(data);
       this.props.history.push("/home/all");
       this.props.login();
    //    window.location="/home";
       }catch (ex){
           let errMsg = `${ex.response.status} ${ex.response.data}`;
           this.setState({errMsg:errMsg});
       }

    }

    handelSumbit= (e)=>{
        e.preventDefault();
        this.postData("login",this.state.from);
    };

    render(){ //life cycl
        let {email,password} = this.state;
        let {errMsg=null}=this.state;
     return  <div className="container"> 
           <div className="row"> 
           <div className="col-3"></div>
           <div className="col-6 bg-light">
           <h5 className="text-center">Login</h5>
           {errMsg && <span className="text-danger center">{errMsg}.. Check your Email and Password </span>}
            <div className="form-group">
             <lable>Email</lable>
               <input 
               type="text" 
               className="form-control" 
               id="email" name="email" 
               value={email}  
               onChange ={this.handelChange}
               /> 
              
            </div>

            <div className="form-group">
             <lable>Password</lable>
               <input 
               type="password" 
               className="form-control" 
               id="password" name="password" 
               value={password}  
               onChange ={this.handelChange}>
               </input>
            </div>
            <br/>
            <button className="btn btn-primary" onClick={this.handelSumbit}>Sumbit</button>
            </div>
           <div className="col-3"></div>
           </div>
          </div>
    }
}
export default Login;