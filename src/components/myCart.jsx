import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "../Services/service";
import SaleImage from "./Saleimage";


class MyCart extends Component{

     state = {
               myCart: this.props.myCart,
               imgIn:0,
             };
         
    
    handleQty=(n,index)=>{
         let s1 = {...this.state}
        this.props.handleQty(n,index);
        this.setState(s1);
    }  
    checkOuthandel=()=>{
        console.log("checkOut hit url")
        this.props.history.push("/checkOut");
    }  

    changeImgIndex=(arr)=>{
        let s1 = {...this.state}
        let l=s1.imgIn;
        if(l==0){
          s1.imgIn++; 
          this.setState(s1);
        }
        if(l==1){
          s1.imgIn++;
          this.setState(s1);
        }
        if(l==2){
          s1.imgIn=0;
          this.setState(s1);
        }
         
        }

    render(){
        const {myCart,imgIn} =this.state;
        
        let mystyle={
            width:'70%',
            height:'100px',
            borderRadius:'20px'
        }
        return (
            <div className="container">
             <SaleImage />
             <h3 className="text-center">You have {myCart.length} itemas in your Cart</h3>             
             <div className="row m-2">
              <div className="col-10"><h5>Cart Value Rs. {myCart.map(bill => bill.totalPrice).reduce((acc, bill) => bill + acc,0)}</h5> </div>
              <div className="col-2"><button className="btn btn-primary" disabled={myCart.length===0 ? true : false} onClick={()=>this.checkOuthandel()}>Check Out</button></div>
             </div>
             <div className="container">
             <div className="row text-light">
                 <div className="col-3 bg-dark"></div>
                 <div className="col-6 bg-dark">Product</div>
                 <div className="col-2 bg-dark">Quantity</div>
                 <div className="col-1 bg-dark">Price</div>
             </div>
             {myCart.map((pr,index)=>(
                 <div className="row border">
                     <div className="col-3"><img style={mystyle} src={pr.imgLink[imgIn]} onLoad={()=>setInterval(()=>this.changeImgIndex(pr.imgLink), 5000)} class="img-fluid" alt="Responsive image"/></div>
                     <div className="col-6">{pr.name}<br/>{pr.category}<br/>{pr.description}</div>
                     <div className="col-2">
                         <div className="row m-1">
                             <div className="col-2"><button className="btn btn-sm btn-success" onClick={()=>this.handleQty("1",index)}>+</button></div>
                             <div className="col-1 text-center">{pr.qty}</div>
                             <div className="col-2"><button className="btn btn-sm btn-warning" onClick={()=>this.handleQty("-1",index)}>-</button></div>
                         </div>
                     </div>
                     <div className="col-1">{pr.totalPrice}</div>
                 </div>
             ))}
             </div>
            </div>
        )
    }
}
export default MyCart;