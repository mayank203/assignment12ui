import React,{Component} from "react";
import {Link} from "react-router-dom";
import {NavDropdown} from "react-bootstrap"
class Navbar extends Component
{
   

    render()
    {
        const {user ,myCart} =this.props;
        console.log(user)
        // let name=user ? user.role==="admin" ? "Admin":user.name[0].toUpperCase()+user.name.substring(1):"";
        return<div className="container-fluid">
                <nav className="navbar navbar-expand-sm bg-dark navbar-light px-2 fw-bold">
                    <Link className="navbar-brand text-light fs-3" to="/home/all">MyStore</Link>
                    <div className="collapse navbar-collapse  fs-5">
                        <ul className="navbar-nav ">
                            <li className="nav-item">
                                <Link className="nav-link  mx-1 text-light" to="/home/Watches">Watches</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link text-light mx-1" to="/home/Sunglasses">Sunglasses</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link text-light mx-1" to="/home/Belts">Belts</Link>
                            </li><li className="nav-item">
                                <Link className="nav-link text-light mx-1" to="/home/Handbags">Handbags</Link>
                            </li>
                            <li className="nav-item mx-2">
                                <NavDropdown className="bg-light" title="Footware">
                                    <NavDropdown.Item>
                                        <Link className="dropdown-item" to="/home/Formal Shoes">Formal Shoes</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item>
                                        <Link className="dropdown-item" to="/home/Sport Shoes">Sport Shoes</Link>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item>
                                        <Link className="dropdown-item" to="/home/Floaters">Floaters</Link>
                                    </NavDropdown.Item><NavDropdown.Item>
                                        <Link className="dropdown-item" to="/home/Sandals">Sandals</Link>
                                    </NavDropdown.Item>
                                </NavDropdown>
                            </li>
                        </ul>


                        <ul className="navbar-nav ms-auto">
                        {!user &&
                            <li className="nav-item">
                                <Link className="nav-link mx-1 text-light" to="/login">Login</Link>
                            </li>}
                        {user &&
                            <li className="nav-item mx-2">
                            <NavDropdown className="bg-light" title={user.email}>
                                <NavDropdown.Item>
                                    <Link className="dropdown-item" to="/myOrders">My Orders</Link>
                                </NavDropdown.Item>
                                <NavDropdown.Item>
                                    <Link className="dropdown-item" to="/manageProducts">Manage Products</Link>
                                </NavDropdown.Item>
                                <hr/>
                                <NavDropdown.Item>
                                    <Link className="dropdown-item" to="/logout">Logout</Link>
                                </NavDropdown.Item>
                            </NavDropdown>
                        </li>
                        }
                        <li className="nav-item">
                                <Link className="nav-link mx-1 text-light" to="/myCart">Cart<span className="bg-success mx-2">{myCart.length}</span></Link>
                        </li>   
                        </ul>
                    </div>
                </nav>
        </div>
    }
}
export default Navbar;