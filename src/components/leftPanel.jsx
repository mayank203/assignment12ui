import React, { Component } from "react";
// import queryString from "query-string";
import { Link } from "react-router-dom";


class LeftPanel extends Component{
    state = { 
            };
    
    handelUrl =(url)=>{
        console.log(url)
        this.props.history.push(`/home/${url}`);
    }

    render(){
        
        return ( <React.Fragment>
                <div className="container border bg-light">
                    <h5 className="m-1">All</h5>
                </div>
                <div className="container border ">
                    <h5 className="m-2" onClick={()=>this.handelUrl("Sunglasses")}>Sunglasses</h5>
                </div>
                <div className="container border ">
                    <h5 className="m-2" onClick={()=>this.handelUrl("Watches")}>Watches</h5>
                </div>
                <div className="container border ">
                    <h5 className="m-2" onClick={()=>this.handelUrl("Belts")}>Belts</h5>
                </div>
                <div className="container border ">
                    <h5 className="m-2" onClick={()=>this.handelUrl("Handbags")}>Handbags</h5>
                </div>
                <div className="container border ">
                    <h5 className="m-2" onClick={()=>this.handelUrl("Wallets")}>Wallets</h5>
                </div>
                <div className="container border ">
                    <h5 className="m-2" onClick={()=>this.handelUrl("Formal Shoes")}>Formal Shoes</h5>
                </div>
                <div className="container border ">
                    <h5 className="m-2" onClick={()=>this.handelUrl("Sports Shoes")}>Sports Shoes</h5>
                </div>
                <div className="container border ">
                    <h5 className="m-2" onClick={()=>this.handelUrl("Floaters")}>Floaters</h5>
                </div>
                <div className="container border ">
                    <h5 className="m-2" onClick={()=>this.handelUrl("Sandals")}>Sandals</h5>
                </div>
                </React.Fragment>
        )
    }
}
export default LeftPanel;