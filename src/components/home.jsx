import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "../Services/service";
// import auth from "../StudentFacultyServices/StudentFacAuth";
import SaleImage from "./Saleimage";
import LeftPanel from "./leftPanel";

class home extends Component{
     state = {
               data:[],
               myCart:this.props.myCart,
               imgIn:0,
             };
    
    async getProductData(){
        const {category} = this.props.match.params;
        console.log(category)
        if(category==="all"){
          let responce = await http.get(`products`);
          let {data} = responce;
          console.log(data);
          this.setState({data:data});
        }
        else {
        let responce = await http.get(`products/${category}`);
        let {data} = responce;
        console.log(data);
        this.setState({data:data});
      }
    }
    componentDidMount(){
        this.getProductData();
    };

    componentDidUpdate(prevProps, precState){
      if(prevProps!==this.props){
        this.getProductData()
      }
    }

    handeladdToCart=(index)=>{
       let items = {qty:1,totalPrice:this.state.data[index].price,...this.state.data[index]};
       this.props.AddToCart(items);
    }
    handelRemoveCart =(id)=>{
      this.props.RemoveCart(id);
    }

    handleQty=(n,id)=>{
      let s1 = {...this.state}
     this.props.handleQtyhome(n,id);
     this.setState(s1);
    }  
     
    render(){
        const {data,myCart,imgIn} =this.state;
        console.log(myCart)
        return (
            <div className="container">
             <SaleImage  /> 
            <div className="row">
                <div className="col-2">
                    <br/>
                  <LeftPanel {...this.props} />
                </div>
                <div className="col-10">
                    <br/>
                    <div className="row">
                      {data.map((pr,index)=>(
                          <div className="col-3 m-2 border">
                              <img src={pr.imgLink[0]} class="img-fluid" alt="Image Note found"/>
                              <h6>{pr.name}</h6>
                              <p>Rs. {pr.price}</p>
                              <p>{pr.description}</p>
                              <div className="d-grid gap-2">
                               {myCart.find((p,i)=>(p.id===pr.id)) ?  
                              //  <button className="btn btn-warning btn-block" onClick={()=>this.handelRemoveCart(pr.id)}>Remove to cart</button>
                              <div style={{display: 'flex',  justifyContent:'center', alignItems:'center',}} className="row m-2 btn-center">
                                  <div className="col-2"><button className="btn btn-sm btn-success" onClick={()=>this.handleQty("1",pr.id)}>+</button></div>
                                  <div className="col-1 text-center">{myCart[myCart.findIndex((p,index)=>p.id===pr.id)].qty}</div>
                                  <div className="col-2"><button className="btn btn-sm btn-warning" onClick={()=>this.handleQty("-1",pr.id)}>-</button></div>
                              </div>
                                :
                              <button className="btn btn-primary btn-block" onClick={()=>this.handeladdToCart(index)}>Add to cart</button>}
                              </div>
                          </div>
                      ))}
                    </div>
                </div>
            </div>  

            </div>
        )
    }
}
export default home;