import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "../Services/service";
import SaleImage from "./Saleimage";
import auth from "../Services/authServices";



class CheckOut extends Component{

     state = {
               myCart: this.props.myCart,
               customer : {name:"",address:"",city:""},
               
             };
         
    handelChange = (e)=>{
        const {currentTarget : input} =e;
        let s1 = {...this.state};
        s1.customer[input.name]=input.value;
        this.setState(s1);
    }; 
   handelSumbit= (e)=>{
    e.preventDefault();
    const s1= {...this.state.customer}
    const s2={...this.state}
    const user = auth.getUser();
    let totalPrice = s2.myCart.map(bill => bill.totalPrice).reduce((acc, bill) => bill + acc,0)
    let obj = {name:s1.name,address:s1.address,city:s1.city,items:s2.myCart,totalPrice:totalPrice,email:user.email} ;
    console.log(obj);
    this.postData(`orders`,obj);
   };

   async postData(url, obj) {
    console.log(url,obj);
    if(obj.name && obj.address && obj.city){
    let response =await http.post(url,obj);
    let {data} = response;
    console.log(data);
    window.location="/Thanks";
    // this.props.history.push("/Thanks"); 
    }else{
      this.setState({err : "Fill all Fileds (*) First before Sumbit"});
    }
}
        

    render(){
        const {myCart} =this.state;
        let {name,address,city} = this.state.customer;
        
        return (
            <div className="container">
             <SaleImage />
             <div className="border">
             <h3 className="text-center">Summry of your Order</h3> 
             <br/>            
             <h5 className="text-center">you have {myCart.length} items</h5>             
             <div className="container text-center">
             <div className="row text-light m-2">
                 <div className="col-4 bg-dark">Name</div>
                 <div className="col-4 bg-dark">Quantity</div>
                 <div className="col-4 bg-dark">Value</div>
             </div>
             {myCart.map((pr,index)=>(
                <div className="row">
                 <div className="col-4 border">{pr.name}</div>
                 <div className="col-4 border">{pr.qty}</div>
                 <div className="col-4 border">Rs.{pr.totalPrice}</div>
             </div>
             ))}
             </div>
             </div>
             <br/>
             <h2 className="text-center">Delivery Details</h2> 
             <div className="container">
             <div className="form-group">
            <lable>Name <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Name"
               className="form-control" 
               id="name" name="name" 
               value={name}  
               onChange ={this.handelChange}>
               </input>
            </div> 
            <div className="form-group">
            <lable>Address <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter address"
               className="form-control" 
               id="address" name="address" 
               value={address}  
               onChange ={this.handelChange}>
               </input>
            </div> 
            
            <div className="form-group">
            <lable>City <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter City"
               className="form-control" 
               id="city" name="city" 
               value={city}  
               onChange ={this.handelChange}>
               </input>
            </div> 
            <button className="btn btn-primary m-2" onClick={this.handelSumbit}>Submit</button>
             </div>
            </div>
            
        )
    }
}
export default CheckOut;




// {
//     "name": "name299jh",
//     "address": "details address",
//     "city": "name of city",
//     "totalPrice": "total price of that order",
//     "items": ["iteams"],
//     "email": "email"
// }