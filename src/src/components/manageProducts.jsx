import React, {Component} from "react";
import http from "../Services/service";
import auth from "../Services/authServices";
import { Link } from "react-router-dom";


class ManageProducts extends Component{
    state ={
        data:[],
    }

    async getProductData(){
        let responce = await http.get(`products`);
        let {data} = responce;
        console.log(data);
        this.setState({data:data});
    };

    componentDidMount(){
        this.getProductData();
    };

    componentDidUpdate(prevProps, precState){
      if(prevProps!==this.props){
        this.getProductData()
      }
    }

    handelDelete=(id)=>{
        this.deletePro(`product/${id}`);
    }

    handelChange =(e)=> {
        console.log("keyup")
        let txt = e.currentTarget.value
        if(txt){
        let s1 = {...this.state}
        s1.data = s1.data.filter((w1)=>w1.name.startsWith(txt))
        console.log("Filter By Search")
        this.setState(s1);
        }else{
            console.log("Complete Data")
            this.getProductData();  
        }
    }

    async deletePro(url){
        let response =await http.deleteReq(url);
        let {data} = response;
        console.log(data);
    }

    handelEdit=(id)=>{
        this.props.history.push(`/editProduct/${id}`);
    }

    AddNewProduct=()=>{
       this.props.history.push("/addProduct");
    }

    render(){
        let {data} = this.state
        // let handelChange=(e)=>{
        //     console.log("key")
        //     let txt = e.currentTarget.value
        //     data = data.filter((w1)=>w1.name.startsWith(txt))
        //     console.log(data)
        // }
        return(
            <div className="container  m-6">
                <br/>
                <br/>
               <button className="btn btn-primary" onClick={()=>this.AddNewProduct()}>Add a Product</button>
               <br/>
               <br/>
            <div className="form-group">
               <input 
               type="text" 
               placeholder="Search by name..... "
               className="form-control" 
               id="address" name="address" 
               onKeyUp={this.handelChange}>
               </input>
            </div> 
            <br/>
            <h5 className="text">Showimg products 1-{data.length}</h5>
            <div className="row bg-dark ">
                <div className="col-1 border text-light">#</div>
                <div className="col-3 border text-light">Title</div>
                <div className="col-2 border text-light">Category</div>
                <div className="col-3 border text-light">Price</div>
                <div className="col-3 border text-light"></div>

            </div>
            {data.map((pr,index)=>(
                <div className="row">
                <div className="col-1 border">{pr.id}</div>
                <div className="col-3 border">{pr.name}</div>
                <div className="col-2 border">{pr.category}</div>
                <div className="col-3 border">{pr.price}</div>
                <div className="col-3 border">
                   <Link onClick={()=>this.handelEdit(pr.id)}>Edit</Link> <Link onClick={()=>this.handelDelete(pr.id)}>Delete</Link>
                </div>

            </div>
            ))}
            </div>
        );
    }
}

export default ManageProducts;