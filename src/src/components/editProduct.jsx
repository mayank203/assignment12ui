import React, {Component} from "react";
import auth from "../Services/authServices";
import http from "../Services/service";


class EditProduct extends Component {
    state ={
      Product : {name: "",price: "",category: "",imgLink: "",description: ""},
    };

    async getProductData(){
        const {id} = this.props.match.params;
        let response = await http.get(`product/${id}`);
        let {data} = response;   
        console.log(data);
        this.setState({Product:data,id:id});
      }
  
       componentDidMount(){
          this.getProductData();
      };
  
      componentDidUpdate(prevProps, precState){
        if(prevProps!==this.props){
          this.getProductData()
        }
      }
    
    handelChange = (e)=>{
        const {currentTarget : input} =e;
        let s1 = {...this.state};
        s1.Product[input.name]=input.value;
        this.setState(s1);
    };   

    async putData(url, obj) {
        console.log(url,obj);
        if(obj.name && obj.imgLink && obj.description && obj.price && obj.category){
        let response =await http.put(url,obj);
        let {data} = response;
        console.log(data);
        this.props.history.push("/manageProducts"); 
        }else{
          this.setState({err : "Fill all Fileds (*) First before Sumbit"});
        }
    }

    handelSumbit= (e)=>{
        e.preventDefault();
        const s1= {...this.state}
        let obj = s1.Product;
       this.putData(`product/${s1.id}`,obj);
    };

    render(){
        let {name,description,price,imgLink,category} = this.state.Product;
        let {err} = this.state;
        let mystyle={
            width:'96%',
            height:'270px',
        }
     return  <div className="container">
             <h2 className="text-center m-4">Edit Product</h2>
             <div className="row">
             <div className="col-2"></div>   
             <div className="col-4 bg-dark">
             <img style={mystyle} src={imgLink} className=" m-2 img-fluid" alt="Responsive image"/>
             <h3 className="text-light m-2">{name}</h3>
             <h6 className="text-light m-2">Category : {category}</h6>
             <h6 className="text-light m-2">Price : Rs.{price}</h6>

             </div>   
             <div className="col-5"> 
            {err && <span className="text-danger">{err}</span> }
            <div className="form-group m-2">
            <lable>Name <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Product Name"
               className="form-control" 
               id="name" name="name" 
               value={name}  
               onChange ={this.handelChange}>
               </input>
            </div>    
            <div className="form-group m-2">
             <lable>Description<span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Product Description"
               className="form-control" 
               id="description" name="description" 
               value={description}  
               onChange ={this.handelChange}>    
               </input>
            </div>

            <div className="form-group m-2">
             <lable>Price <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Product Price"
               className="form-control" 
               id="price" name="price" 
               value={price}  
               onChange ={this.handelChange}>    
               </input>
            </div>
            <div className="form-group m-2">
            <lable>Image Url <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Product Image URL"
               className="form-control" 
               id="imgLink" name="imgLink" 
               value={imgLink}  
               onChange ={this.handelChange}>
               </input>
            </div>
            <div className="form-group m-2">
            <lable> Category <span className="text-danger">*</span> :</lable>
               <input 
               type="text" 
               placeholder="Enter Product category"
               className="form-control" 
               id="category" name="category" 
               value={category}  
               onChange ={this.handelChange}>
               </input>
            </div>
            <button className="btn btn-primary m-2" onClick={this.handelSumbit}>Save</button>
            <br/>
            </div> 
            <div className="col-1"></div> 
            </div>
          </div>
    }
}
export default EditProduct;