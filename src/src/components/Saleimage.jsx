import React, {Component} from "react";
class SaleImage extends Component{
    render(){
        const mystyle = {
            backgroundColor: "white",
            width:'100%',
            height:'200px',
           };
        return(
            <div className="container text-center"> 
            <img style={mystyle} src="https://github.com/edufectcode/react/blob/main/data/MyStore-sale.jpg?raw=true" class="img-fluid" alt="Responsive image"/>
        </div>
        );
    }
}

export default SaleImage;